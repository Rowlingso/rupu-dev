<?php //include('../php/header.php'); ?>
<?php //include('../config/config.php'); ?>

<div class="title"><center>
<b><a href="birthday_room.php">Reserve a Private Room</a></b> | <a href="FAQ.php">F.A.Q.</a><br>
</center></div> <div class="box"> 

<?php

/*if (!mysql_connect(localhost, $db_username, $db_password))
    die("Can't connect to database");
if (!mysql_select_db($db_name))
    die("Can't select database");*/

$expire = date("Y-m-d H:i:s", strtotime('+48 hours'));
$expire2 = date("Y-m-d h:i A", strtotime('+48 hours'));

$birth_day=test_input($_REQUEST['birth_day']);
$birth_month=test_input($_REQUEST['birth_month']);
$birth_year=test_input($_REQUEST['birth_year']);
$child_bd= $birth_year . "-" .$birth_month . "-" .$birth_day;
$party_date=test_input($_REQUEST['party_date']);
$party_date = date("Y-m-d",strtotime($party_date));
$party_time=test_input($_REQUEST['party_time']);
$room=test_input($_REQUEST['room']);
$tble=$_REQUEST['tble'];
$tr=$_REQUEST['tr'];
$child=test_input($_REQUEST['child']);
$child_age=test_input($_REQUEST['child_age']);
$parent=test_input($_REQUEST['parent']);
$parent_address=test_input($_REQUEST['parent_address']);
$parent_city=test_input($_REQUEST['parent_city']);
$parent_zip=test_input($_REQUEST['parent_zip']);
$phone=test_input($_REQUEST['phone']);
$email=test_input($_REQUEST['email']);
$package=test_input($_REQUEST['package']);
$kids=test_input($_REQUEST['kids']);
$source=test_input($_REQUEST['source']);
$comment=test_input($_REQUEST['comment']);
$paid=test_input($_REQUEST['paid']);

/*$get=mysql_query("SELECT MAX(id) FROM reserve"); 
$got = mysql_fetch_array($get); 
$next_id = $got['MAX(id)'] + 1;*/


//function to sanitize the data before saving. prevents sql injections
function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}


echo "<br><b>Please review your private birthday party room reservation</b><br>";
echo "Party Date: $party_date<br>";
echo "Party Time: $party_time<br>";
echo "Child's Name: $child<br>";
echo "Child's Age: $child_age<br>";
echo "Child's Birthday: $child_bd<br>";
echo "Parent's Name: $parent<br>";
echo "Parent's Address: $parent_address, $parent_city $parent_zip<br>";
echo "Parent's Phone: $phone<br>";
echo "Parent's email: $email<br>";
echo "Selected Birthday Package: $package<br>";
echo "Number of Kids: $kids<br>";
echo "How did you hear about us: $source<br>";
echo "Comment: $comment<br>";
echo "Jambo! Ambassador: $tble<br>";
echo "Reservation expires: $expire2<br><br>";

echo "You are reserving a private room that can accomodate 32 guests for 2 hours. You are placing a 48 hour courtesy hold on your room. Please contact us at (602) 274-4653 to secure your private room. You are also agreeing to our rules: <br>";

echo "* 10 guest minimum<br>";
echo "* Children must be supervised at all times<br>";
echo "* No running or climbing<br>";
echo "* Socks required in playground<br>";
echo "* Outside food and beverages are not allowed<br>";
echo "* Visa, Mastercard, American Express and Discover Card accepted<br>";
echo "* Cash or credit card only, sorry, no checks accepted<br>";
echo "* All prices are exclusive of local sales taxes<br>";
echo "* Prices are subject to change at any time without notice<br>";

echo "<form action=birthday_room3.php method=post>";
echo "<input type=hidden name='next_id' value='$next_id'>";
echo "<input type=hidden name='expire' value='$expire'>";
echo "<input type=hidden name='expire2' value='$expire2'>";
echo "<input type=hidden name='paid' value='no'>";
echo "<input type=hidden name='tr' value='r'>";
echo "<input type=hidden name='party_date' value='$party_date'>";
echo "<input type=hidden name='party_time' value='$party_time'>";
echo "<input type=hidden name='room' value='$room'>";
echo "<input type=hidden name='tble' value='$tble'>";
echo "<input type=hidden name='child' value='$child'>";
echo "<input type=hidden name='child_age' value='$child_age'>";
echo "<input type=hidden name='child_bd' value='$child_bd'>";
echo "<input type=hidden name='parent' value='$parent'>";
echo "<input type=hidden name='parent_address' value='$parent_address'>";
echo "<input type=hidden name='parent_city' value='$parent_city'>";
echo "<input type=hidden name='parent_zip' value='$parent_zip'>";
echo "<input type=hidden name='phone' value='$phone'>";
echo "<input type=hidden name='email' value='$email'>";
echo "<input type=hidden name='package' value='$package'>";
echo "<input type=hidden name='kids' value='$kids'>";
echo "<input type=hidden name='source' value='$source'>";
echo "<input type=hidden name='comment' value='$comment'>";
echo "<br><br><input type=checkbox name='' value=''> I agree with these rules.<br><br>";
echo "<input type=Submit value='Reserve Room with 48 hour courtesy hold'>";
echo "</form>";

echo "You can also secure your private room now by paying the $30 deposit via Paypal.<br>";

echo "<form action=birthday_room4.php method=post>";
echo "<input type=hidden name='next_id' value='$next_id'>";
echo "<input type=hidden name='expire' value='$expire'>";
echo "<input type=hidden name='expire2' value='$expire2'>";
echo "<input type=hidden name='paid' value='no'>";
echo "<input type=hidden name='tr' value='r'>";
echo "<input type=hidden name='party_date' value='$party_date'>";
echo "<input type=hidden name='party_time' value='$party_time'>";
echo "<input type=hidden name='room' value='$room'>";
echo "<input type=hidden name='tble' value='$tble'>";
echo "<input type=hidden name='child' value='$child'>";
echo "<input type=hidden name='child_age' value='$child_age'>";
echo "<input type=hidden name='child_bd' value='$child_bd'>";
echo "<input type=hidden name='parent' value='$parent'>";
echo "<input type=hidden name='parent_address' value='$parent_address'>";
echo "<input type=hidden name='parent_city' value='$parent_city'>";
echo "<input type=hidden name='parent_zip' value='$parent_zip'>";
echo "<input type=hidden name='phone' value='$phone'>";
echo "<input type=hidden name='email' value='$email'>";
echo "<input type=hidden name='package' value='$package'>";
echo "<input type=hidden name='kids' value='$kids'>";
echo "<input type=hidden name='source' value='$source'>";
echo "<input type=hidden name='comment' value='$comment'>";
echo "<input type='image' src='https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif' border='0' name='submit' alt='PayPal - The safer, easier way to pay online!'>";
echo "<img alt='' border='0' src='https://www.paypalobjects.com/en_US/i/scr/pixel.gif' width='1' height='1'>";
echo "</form>";

?>

</table>
</div>
</div>

<?php //include('../php/footer.php'); ?>

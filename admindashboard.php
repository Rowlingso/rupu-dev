<?php
session_start();

if(!$_SESSION['logged_in']){
	header('Location: '. url().'/index.php?page=act/login&login=1' );
}
require_once 'classes/super_admin_dashboard.php';

if(isset($_GET['action']))
{
	if($_GET['action']=='logout')
		SuperAdminDashboard::logout();
}

function url(){
  return sprintf(
    "%s://%s%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME'],
    dirname($_SERVER['PHP_SELF'])
  );
}
?>
<head>
<script type="text/javascript" src="js/jquery-1.10.2.js"></script> 
<script type="text/javascript" src="js/jquery.tablesorter.js"></script> 
<script>
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
); 
</script>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body>

<div class="links">
<a href="?view=listaffiliates">Affilites</a>
<a href="?view=listclicks">Clicks</a>
<a href="?view=generalstats">General Statistics</a>
<a href="?view=listpayments">Payments</a>
<a href="?action=logout">Logout</a>
</div>

<?php 
	if(isset($_GET['view'])){

	if($_GET['view'] == "listaffiliates"){
			$ambassadors = SuperAdminDashboard::fetchAmbassadors();


		?>
<div id="wrap">

<h3>Affiliates</h3>

<table border="1"  id="myTable" class="tablesorter">
	<thead>
	<tr>
	<th>Username</th>
	<th>First Name</th>
	<th>Last Name</th>
	<th>Organization</th>
	<th>Address</th>
	<th>Phone Number</th>
	<th>Email</th>
	<th>Verified Email</th>
	<th>Ambassadors Applications</th>
	</tr>
</thead>
<tbody>
	<?php foreach($ambassadors as $ambassador){ ?>
	<tr>
	<td> <?php echo $ambassador['usrnam'];?></td>
	<td> <?php echo $ambassador['usrfnam'];?></td>
	<td> <?php echo $ambassador['usrlnam'];?></td>
	<td> <?php echo $ambassador['usrorg'];?></td>
	<td> <?php echo $ambassador['usraddress'];?></td>
	<td> <?php echo $ambassador['usrphone'];?></td>
	<td> <?php echo $ambassador['usremail'];?></td>

	<?php if($ambassador['confirmflag']==0) { ?>
	<td>Unverified </td>
	<?php }else{?>
	<td>Email Verified</td>
	<?php } ?>

	<?php if($ambassador['approve']==0) { ?>
	<td><a href="admindashboard.php?id=<?php echo $ambassador['usrid'];?>&token=<?php echo $ambassador['token'];?>&action=approve" title="Click here to approve the application" class="btn">Unapproved </a></td>
	<?php }elseif($ambassador['approve']==1){?>
	<td><a href="admindashboard.php?id=<?php echo $ambassador['usrid'];?>&token=<?php echo $ambassador['token'];?>&action=suspend" title="Click here to suspend the application" class="btn">Approved</a></td>
	<?php }else{ ?>
	<td><a href="admindashboard.php?id=<?php echo $ambassador['usrid'];?>&token=<?php echo $ambassador['token'];?>&action=approve" title="Click here to reapprove the application" class="btn">Suspended</a></td>
	<?php }?>

	
	</tr>

	<?php }?>
</tbody>

	<?php if(isset($_GET['id']) && isset($_GET['token'])) {?>

	<?php 

	if($_GET['action'] == 'approve')
	SuperAdminDashboard::approveAmbassador($_GET['id'],$_GET['token']);
	elseif ($_GET['action'] == 'suspend') {
	 SuperAdminDashboard::suspendAmbassador($_GET['id'],$_GET['token']);
	 } ?>

	<?php header('location http://localhost/affiliate/admindashboard.php' ); ?>

	<?php }?>



</table>
</div>
<!--end if view = affiliates -->
<?php }?>
	<?php

	$clicks = SuperAdminDashboard::displayClicks();

	//echo '<pre>';var_dump($clicks);exit;

	if($_GET['view']=='listclicks') {?>

	<div id="wrap">

	<h3>Clicks</h3>

	<table border="1" id="myTable" class="tablesorter">
		<thead>
		<tr>
			<th>Time</th>
			<th>Affiliate</th>
			<th>URL</th>
			<th>IP</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($clicks as $click) { ?>

			<tr>
			<td> <?php echo $click['time'];?></td>
			<td> <?php echo $click['usrfnam'] .' '. $click['usrlnam'];?></td>
			<td> <?php echo '-';?></td>
			<td> <?php echo $click['click_ip'];?></td>


		<?php } ?>
	</tbody>
	</table>
</div>
<!--end if view = clicks -->
	<?php } ?>

	<?php

	//echo '<pre>';var_dump($clicks);exit;

	if($_GET['view']=='generalstats') {

		if(isset($_GET['sort']))
			$generalstats = SuperAdminDashboard::getGeneralStats($_GET['sort']);
		else
			$generalstats = SuperAdminDashboard::getGeneralStats();
	?>

	<div id="wrap">

	<h3>General Statistics</h3>

	<table border="1" id="myTable" class="tablesorter">
		<thead>
		<tr>
			<th>Ambassador</th>
			<th>Total Clicks</th>
			<th>Reservations</th>
			<th>Paid Deposits</th>
			<th>Commissions</th>
			<th>Payment Sent</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($generalstats as $generalstat) { ?>

			<tr>
			<td> <?php echo $generalstat['usrnam'];?></td>
			<td> <?php echo $generalstat['clickcount'];?></td>
			<td> <?php echo $generalstat['reservationscount'];?></td>
			<td> <?php echo $generalstat['paidcount'];?></td>
			<td> <?php echo $generalstat['commissions'];?></td>
			<td> <?php echo $generalstat['paymentsent'];?></td>


		<?php } ?>
	</tbody>
	</table>
</div>
<!--end if view = generalstats -->
	<?php } ?>



	<?php $payments = SuperAdminDashboard::displayPayments(); //echo '<pre>';var_dump($payments);exit;

	if($_GET['view']=='listpayments'){ ?>

	<div id="wrap">


	<h3>Payments</h3>

	<table border="1" id="myTable" class="tablesorter">
		<thead>
		<tr>
			<th>Time</th>
			<th>Amount</th>
			<th>Affiliate</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($payments as $payment) { ?>

		<tr>
			<td> <?php echo $payment['time'];?></td>
			<td> <?php echo $payment['amount'];?></td>
			<td> <?php echo $payment['usrfnam'] .' '. $payment['usrlnam'];?></td>
		</tr>


		<?php } ?>
	</tbody>
	</table>

</div>

	<!--end if view = payments -->
	<?php } ?>

<!-- end if(isset($_GET['view']) -->
<?php } ?>
</body>
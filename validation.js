
  function validate(){
    var valid = true;
      var msg="Error in form:\n";
      if (document.getElementById("kids").value < 10) {
        msg+="Number of children must be at least 10!\n";
        valid = false;
      }
       if (document.getElementById("party_date").value == "") {
		    msg+="Please fill in the party date!\n";
		    valid = false;
		  }
		  if (document.getElementById("child").value == "") {
			    msg+="Please fill in the child name!\n";
			    valid = false;
			  }
		  if (document.getElementById("child_age").value == "") {
			    msg+="Please fill in the child age!\n";
			    valid = false;
			  }
		  if(!validateEmail(email)){
			  msg+="Please provide valid email address!\n";
				valid = false;
			}
		  if (document.getElementById("parent").value == "") {
			    msg+="Please fill in the parent's name!\n";
			    valid = false;
			  }
		  if (document.getElementById("parent_address").value == "") {
			    msg+="Please fill in the parent's address!\n";
			    valid = false;
			  }
		  if (document.getElementById("parent_city").value == "") {
			    msg+="Please fill in the parent's city!\n";
			    valid = false;
			  }
		  if (document.getElementById("parent_zip").value == "") {
			    msg+="Please fill in the parent's zip!\n";
			    valid = false;
			  }
		  if (document.getElementById("phone").value == "") {
			    msg+="Please fill in the phone number!\n";
			    valid = false;
			  }
		  if (document.getElementById("source").value == "") {
			    msg+="Please fill how you found us!\n";
			    valid = false;
			  }
		  if (document.getElementById("comment").value == "") {
			    msg+="Please leave a comment!\n";
			    valid = false;
			  }
      if (!valid){ alert(msg);
      	return false;
      }
      else
      document.forms[form.name].submit();

  }

  function validateEmail(email) { 
  		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
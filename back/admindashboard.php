<?php
session_start();

require_once 'classes/super_admin_dashboard.php';

if(!$_SESSION['logged_in']){
//echo 'here';exit;
	header('Location: http://localhost/affiliate/index.php?page=act/login&login=1' );
}

if(isset($_GET['action']))
{
	if($_GET['action']=='logout')
		SuperAdminDashboard::logout();
}

//echo '<pre>';var_dump($ambassadors);exit;




?>

<a href="?view=listaffiliates">Affilites</a>
<a href="?view=listclicks">Clicks</a>
<a href="?view=generalstats">General Statistics</a>
<a href="?view=listpayments">Payments</a>
<a href="?action=logout">Logout</a>

<?php 
	if(isset($_GET['view'])){

	if($_GET['view'] == "listaffiliates"){

		if(isset($_GET['sort']))
			$ambassadors = SuperAdminDashboard::fetchAmbassadors($_GET['sort']);
		else
			$ambassadors = SuperAdminDashboard::fetchAmbassadors();


		?>

<h3>Affiliates</h3>

<table border="1">
	<tr>
	<th><a href="?view=listaffiliates&sort=usrnam">Username</a></th>
	<th><a href="?view=listaffiliates&sort=usrfnam">First Name</a></th>
	<th><a href="?view=listaffiliates&sort=usrlnam">Last Name</a></th>
	<th><a href="?view=listaffiliates&sort=usrorg">Organization</a></th>
	<th><a href="?view=listaffiliates&sort=usraddress">Address</a></th>
	<th><a href="?view=listaffiliates&sort=usrphone">Phone Number</a></th>
	<th><a href="?view=listaffiliates&sort=usremail">Email</a></th>
	<th>Verified Email</th>
	<th>Ambassadors Applications</th>
	</tr>
	<?php foreach($ambassadors as $ambassador){ ?>
	<tr>
	<td> <?php echo $ambassador['usrnam'];?></td>
	<td> <?php echo $ambassador['usrfnam'];?></td>
	<td> <?php echo $ambassador['usrlnam'];?></td>
	<td> <?php echo $ambassador['usrorg'];?></td>
	<td> <?php echo $ambassador['usraddress'];?></td>
	<td> <?php echo $ambassador['usrphone'];?></td>
	<td> <?php echo $ambassador['usremail'];?></td>

	<?php if($ambassador['confirmflag']==0) { ?>
	<td>Unverified </td>
	<?php }else{?>
	<td>Email Verified</td>
	<?php } ?>

	<?php if($ambassador['approve']==0) { ?>
	<td><a href="admindashboard.php?id=<?php echo $ambassador['usrid'];?>&token=<?php echo $ambassador['token'];?>&action=approve" title="Click here to approve the application">Unapproved </a></td>
	<?php }elseif($ambassador['approve']==1){?>
	<td><a href="admindashboard.php?id=<?php echo $ambassador['usrid'];?>&token=<?php echo $ambassador['token'];?>&action=suspend" title="Click here to suspend the application">Approved</a></td>
	<?php }else{ ?>
	<td><a href="admindashboard.php?id=<?php echo $ambassador['usrid'];?>&token=<?php echo $ambassador['token'];?>&action=approve" title="Click here to reapprove the application">Suspended</a></td>
	<?php }?>

	
	</tr>

	<?php }?>

	<?php if(isset($_GET['id']) && isset($_GET['token'])) {?>

	<?php 

	if($_GET['action'] == 'approve')
	SuperAdminDashboard::approveAmbassador($_GET['id'],$_GET['token']);
	elseif ($_GET['action'] == 'suspend') {
	 SuperAdminDashboard::suspendAmbassador($_GET['id'],$_GET['token']);
	 } ?>

	<?php header('location http://localhost/affiliate/admindashboard.php' ); ?>

	<?php }?>



</table>
<!--end if view = affiliates -->
<?php }?>
	<?php

	$clicks = SuperAdminDashboard::displayClicks();

	//echo '<pre>';var_dump($clicks);exit;

	if($_GET['view']=='listclicks') {?>

	<h3>Clicks</h3>

	<table border="1">
		<tr>
			<th>Time</th>
			<th>Affiliate</th>
			<th>URL</th>
			<th>IP</th>
		</tr>
		<?php foreach ($clicks as $click) { ?>

			<tr>
			<td> <?php echo $click['time'];?></td>
			<td> <?php echo $click['usrfnam'] .' '. $click['usrlnam'];?></td>
			<td> <?php echo '-';?></td>
			<td> <?php echo $click['click_ip'];?></td>


		<?php } ?>
	</table>
<!--end if view = clicks -->
	<?php } ?>

	<?php

	$clicks = SuperAdminDashboard::displayClicks();

	//echo '<pre>';var_dump($clicks);exit;

	if($_GET['view']=='generalstats') {?>

	<h3>Clicks</h3>

	<table border="1">
		<tr>
			<th>Ambassador</th>
			<th>Total Clicks</th>
			<th>Reservations</th>
			<th>Paid Deposits</th>
			<th>Commissions</th>
			<th>Payment Sent</th>
		</tr>
		<?php foreach ($clicks as $click) { ?>

			<tr>
			<td> <?php echo $click['time'];?></td>
			<td> <?php echo $click['usrfnam'] .' '. $click['usrlnam'];?></td>
			<td> <?php echo '-';?></td>
			<td> <?php echo $click['click_ip'];?></td>
			<td> <?php echo '-';?></td>
			<td> <?php echo $click['click_ip'];?></td>


		<?php } ?>
	</table>
<!--end if view = generalstats -->
	<?php } ?>



	<?php $payments = SuperAdminDashboard::displayPayments(); //echo '<pre>';var_dump($payments);exit;

	if($_GET['view']=='listpayments'){ ?>


	<h3>Payments</h3>

	<table border="1">
		<tr>
			<th>Time</th>
			<th>Amount</th>
			<th>Affiliate</th>
		</tr>
		<?php foreach ($payments as $payment) { ?>

		<tr>
			<td> <?php echo $payment['time'];?></td>
			<td> <?php echo $payment['amount'];?></td>
			<td> <?php echo $payment['usrfnam'] .' '. $payment['usrlnam'];?></td>
		</tr>


		<?php } ?>
	</table>

	<!--end if view = payments -->
	<?php } ?>

<!-- end if(isset($_GET['view']) -->
<?php } ?>
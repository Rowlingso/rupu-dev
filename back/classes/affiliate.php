<?php

require_once 'db_functions.php';

class Affiliate
{
	public static function awardPoints($id,$token)
	{
		$valid = Affiliate::authenticateRequest($id,$token);

		if($valid)
		{
			$saveid = Affiliate::storeToDB($id);

			if($saveid)
			{
				//session_start();
				//store the variables to the session to track if user makes a reservation
				$_SESSION['ambassadorid'] = $id;
				$_SESSION['token'] = $token;
				$_SESSION['name'] = Affiliate::getAmbassadorName($id);

				$home="birthday_room.php";
 				echo "<meta http-equiv='refresh' content='0;URL=$home'>";

 				return $saveid;
			}
		}else{
			return false;
		}

	}

	public static function authenticateRequest($id,$token)
	{
		$result = DBFunctions::select('scraffiliateusr','*','usrid="'.$id.'" and token="'.$token.'"');

		return $result->num_rows;

	}

	public static function getAmbassadorName($id)
	{
		$result = DBFunctions::select('scraffiliateusr','usrnam','usrid="'.$id.'"');

		$row = $result->fetch_array(MYSQLI_ASSOC);

		return $row['usrnam'];
	}

	public static function storeToDB($id)
	{
		$store['aid'] = "''";
		$store['count'] = "'1'";
		$store['usrid'] = "'$id'";
		$store['click_ip'] =  "'$_SERVER[REMOTE_ADDR]'";
		$datetime = date('Y-m-d H:i:s');
		$store['time'] = "'$datetime'";
		$store['paid'] = "0";


		$saveid = DBFunctions::insert('affiliate_count',$store);

		//session_destroy();

		return $saveid;
	}
}
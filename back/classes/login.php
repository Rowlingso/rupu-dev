<?php

require_once 'db_functions.php';

define('ENCRIPTION_KEY', '#$%^&*()(*');

class Login
{
	private static function hashPassword($pure_string, $encryption_key) {
	    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
	    return $encrypted_string;
	}

	public static function validateUserEntries($data)
	{
		$err = "";

		if(!$data['username'])
			$err="Please Enter Your Username<br>";
		if(!$data['password'])
			$err="Please Enter Your Password<br>";

		return $err;
	}

	public static function authenticateUser($data)
	{
		$username = $data['username'];
		$password = Login::hashPassword($data['password'], ENCRIPTION_KEY);

		$result = DBFunctions::select('scraffiliateusr','*','usrnam="'.$username.'" and usrpwd="'.$password.'"');

		$rows = $result->fetch_array(MYSQLI_ASSOC);

		if(count($rows)){
		$_SESSION['active_user'] = $rows;
		$_SESSION['logged_in'] = TRUE;
		}

		return $rows;
	}
}
<?php

require_once 'db_functions.php';

class SuperAdminDashboard
{

  public static function getGeneralStats()
  {
      $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

      $query = "SELECT s.usrnam, from scraffiliateusr as s";
  }

 public static function fetchAmbassadors($orderby='usrid')
 {

 	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		$query = "SELECT s.*, a.approve FROM scraffiliateusr as s left join ambassador_approvals as a on s.usrid = a.ambid order by s.$orderby asc";

    //echo $query;exit;

		$result =	$mysqli->query($query);

 	$ambassadors = array();

 	while($row = mysqli_fetch_array($result)) {
 		$ambassadors[] = $row;
	}

 	//$rows = $ambassadors->fetch_array(MYSQLI_ASSOC);

 	return $ambassadors;
 }

 public static function approveAmbassador($id,$token)
 {
 	$updatevalue = array('approve'=>'1');
 	DBFunctions::update('ambassador_approvals',$updatevalue,'ambid',$id);

  }

   public static function suspendAmbassador($id,$token)
 {
 	$updatevalue = array('approve'=>'2');
 	DBFunctions::update('ambassador_approvals',$updatevalue,'ambid',$id);

  }

  public static function displayPayments()
  {

  	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  	$query = "SELECT s.usrfnam, s.usrlnam, p.* FROM affiliate_payment as p left join scraffiliateusr as s on s.usrid = p.affiliate order by s.usrid desc";


		$result =	$mysqli->query($query);

 	$payments = array();

 	while($row = mysqli_fetch_array($result)) {
 		$payments[] = $row;
	}

	return $payments;

  }

   public static function displayClicks()
  {

    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $query = "SELECT s.usrfnam, s.usrlnam, c.* FROM affiliate_count as c left join scraffiliateusr as s on s.usrid = c.usrid order by s.usrid desc";

    $result = $mysqli->query($query);

  $clicks = array();

  while($row = mysqli_fetch_array($result)) {
    $clicks[] = $row;
  }

  return $clicks;

  }

  public static function logout()
  {
    session_start();
    unset($_SESSION);
    session_destroy();

    header('Location: http://localhost/affiliate/index.php?page=act/login&login=2' );
  }
}
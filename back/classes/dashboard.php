<?php

require_once 'db_functions.php';

class Dashboard
{

	public static function getGeneralStats($userid)
	{

		$clicks = Dashboard::countClicks($userid);

		$stats = array();


		foreach ($clicks as $click) {
			//add reservation count to clicks
			$reservations = Dashboard::countReservations($click['all_dates']);

			$click['reservations'] = $reservations;
			$click['commissions'] = $reservations * 10;

			$stats[] = $click;
		}

		return $stats;
		
	}



	public static function countClicks($userid)
	{
		$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);

		$query = 'SELECT count(clicks) as clicks, DATE(time) as all_dates FROM affiliate_count WHERE usrid = "'.$userid.'" group by DATE(time)';

		$result = $mysqli->query($query);

		$stats = array();

		while ($row = mysqli_fetch_array($result)) {
			$stats[] = $row;
		}

		return $stats;
	}

	public static function countReservations($date)
	{
		$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);

		$query = 'SELECT count(reserves) as reserves FROM reservations WHERE DATE(time) = "'.$date.'" AND paid = "1"';


		$result = $mysqli->query($query);

		$reserve = $result->fetch_array(MYSQLI_ASSOC);

		return $reserve['reserves'];
	}

	public static function getPaymentsHistory($userid)
	{
		$mysqli = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);

		$query = 'SELECT DATE(time) as all_dates, amount FROM affiliate_payment WHERE affiliate = "'.$userid.'" order by DATE(time) desc';

		$result = $mysqli->query($query);

		$stats = array();

		while ($row = mysqli_fetch_array($result)) {
			$stats[] = $row;
		}

		return $stats;
	}


	public static function logout()
	  {
	    session_start();
	    unset($_SESSION);
	    session_destroy();

	    header('Location: http://localhost/affiliate/index.php?page=act/login&login=2' );
	  }

	public static function getBirthdayReservations()
	{

	}

	public static function getCompletedPayments()
	{

	}

	public static function calculateCommissions()
	{

	}

}

?>
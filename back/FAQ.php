<?php include('../php/header.php'); ?>
<a href="birthday_room.php">Reserve a Private Room</a> | <a href="birthday_table.php">Reserve a Table</a> | <b><a href="FAQ.php">F.A.Q.</a></b><br><br>
<b><u>F.A.Q.: Frequently Asked Questions</b></u><br><br>

<b><u>Q: </b></u>What's the difference between a private room reservation and a table reservation?<br>
<b><u>A: </b></u>Our private rooms can hold 32 guests and are available to you for 2 hours. There is a $30 fee and a 10 child minimum to reserve a private room for your birthday party. If you have less than 10 children in your party, you can reserve a hexagon table in our party area that seat 6-8 guests at $15 per table for 90 minutes.<br><br>

<b><u>Q: </b></u>What Birthday Party Packages are available to choose from?<br>
<b><u>A: </b></u><br>
<b>Monkey Birthday Party:</b> $12.99 per Child. Our Monkey Birthday Party guests enjoy unlimited rides (Himalaya Mini Roller Coaster Rides, Monkey Mayhem Barrels Rides, Spinning Tops Rides, Flying Elephant Rides, Jungle Swing Rides, Safari Train Rides), unlimited Jungle themed Laser Tag, unlimited 6 hole Indoor Miniature Golf and unlimited Indoor Playground with slides and tunnels. <br><b>Giraffe Birthday Party:</b> $15.99 per Child. Our Giraffe Birthday Party guests enjoy all of the Monkey Package, plus 2 slices of Pizza per child and fountain soda drink or bottled water per child. 
<br><b>Elephant Birthday Party:</b> $19.99 per Child. Our Elephant Birthday Party guests enjoy all of the Giraffe Package, plus 25 tokens per child, FREE return pass for Birthday child and invitations & thank you cards<br><br>

<b><u>Q: </b></u>Can I bring my own foods & drinks?<br>
<b><u>A: </b></u>No. With the exception of birthday cake and ice cream, outside foods and drinks are NOT allowed at Jambo! Park. However, guests contained in an area where other guests are not exposed to their food (private room) may be allowed to bring their own food (but not drinks) upon prior approval and payment of a $50 fee.<br><br>

<b><u>Q: </b></u>Do I need to buy wristbands for adults in by party?<br>
<b><u>A: </b></u>No. There is no admission fee to enter Jambo! Park. You only need wristbands for those who want to enjoy the rides. There is a $8 Adult wristband that offers you unlimited rides (Himalaya Mini Roller Coaster Rides, Monkey Mayhem Barrels Rides, Spinning Tops Rides), unlimited Jungle themed Laser Tag and unlimited 6 hole Indoor Miniature Golf<br><br>

<b><u>Q: </b></u>Do my kids need socks?<br>
<b><u>A: </b></u>Yes, socks is required on the playground with slides and tunnels.<br><br>

<b><u>Q: </b></u>What age group is Jambo! Park best for?<br>
<b><u>A: </b></u>Jambo! Park has something for every age group but majority of our guests are between the ages of 2-10.<br><br>

<b><u>Q: </b></u>Can my guests still stay and play after our room or table time is over?<br>
<b><u>A: </b></u>Yes, your wristband is good for the entire day. After your room or table time is over, you can just move to the general public seating area.<br><br>



<?php include('../php/footer.php'); ?>

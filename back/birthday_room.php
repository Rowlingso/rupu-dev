<?php //include('../php/header.php'); ?>
<?php //include('../config/config.php');

require_once 'classes/reservations.php';

session_start();

$ambassador=$_SESSION['name'];



if(isset($_GET['action']))
{
	if($_GET['action']=='reserve'){
		$confirmation = Reservations::storeReservations();

		if($confirmation)
		{
			header("Location: http://localhost/affiliate/birthday_room.php?view=reserved");
		}
	}
}

if(isset($_GET['view'])){

	if($_GET['view']=="confirmation"){

	$_SESSION['reservation'] = $_POST;

	$confirmation = Reservations::displayConfirmation($_POST);

	echo $confirmation;

}elseif($_GET['view']=="reserved"){
	echo "Thank you for placing a 48 hour courtesy hold on your private room reservation. Please contact us at (602) 274-4653 to secure your room.";
}	//display default
	}else{
?>

<head>
<script>


  function validate(){
    var valid = true;
      var msg="Error in form:\n";
      if (document.getElementById("kids").value < 10) {
        msg+="Number of children must be at least 10!\n";
        valid = false;
      }
       if (document.getElementById("selected_date").value == "") {
		    msg+="Please fill in the party date!\n";
		    valid = false;
		  }
		  if (document.getElementById("child").value == "") {
			    msg+="Please fill in the child name!\n";
			    valid = false;
			  }
		  if (document.getElementById("child_age").value == "") {
			    msg+="Please fill in the child age!\n";
			    valid = false;
			  }
			  var email = document.getElementById("email").value;
		  if(!validateEmail(email)){
			  msg+="Please provide valid email address!\n";
				valid = false;
			}
		  if (document.getElementById("parent").value == "") {
			    msg+="Please fill in the parent's name!\n";
			    valid = false;
			  }
		  if (document.getElementById("parent_address").value == "") {
			    msg+="Please fill in the parent's address!\n";
			    valid = false;
			  }
		  if (document.getElementById("parent_city").value == "") {
			    msg+="Please fill in the parent's city!\n";
			    valid = false;
			  }
		  if (document.getElementById("parent_zip").value == "") {
			    msg+="Please fill in the parent's zip!\n";
			    valid = false;
			  }
		  if (document.getElementById("phone").value == "") {
			    msg+="Please fill in the phone number!\n";
			    valid = false;
			  }
		  if (document.getElementById("source").value == "") {
			    msg+="Please fill how you found us!\n";
			    valid = false;
			  }
      if (!valid){ alert(msg);
      	return false;
      }
      else
      document.forms[form.name].submit();

  }

  function validateEmail(email) { 
  		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
  </script>
<head>

<div class="title"><center>

<b><a href="birthday_room.php">Reserve a Private Birthday Party Room</a></b> | <a href="FAQ.php">F.A.Q.</a><br>

</center>
<font size=3>
Looks like you were referred by a Jambo! Ambassador. You get $10 off your birthday party at completion of your party. A birthday party adventure at Jambo! Park is fun, memorable & affordable!<br><br>

<b><u>Monkey Birthday Party: $12.99 per Child</b></u><br>
<ul>
	<li>Unlimited Rides (Himalaya Mini Roller Coaster, Monkey Mayhem Barrels Rides, Spinning Tops Rides, Flying Elephant Rides, Jungle Swing Rides, Safari Train)</li>
	<li>Unlimited Jungle themed Laser Tag</li>
	<li>Unlimited 6 hole Indoor Miniature Golf</li>
	<li>Unlimited Indoor Playground with slides and tunnels</li>
</ul>

<b><u>Giraffe Birthday Party: $15.99 per Child</b></u><br>
All of the Monkey Package, plus:
<ul>
	<li>2 slices of Pizza per child</li>
	<li>Fountain Soda Drink or bottled water per child</li>
</ul>

<b><u>Elephant Birthday Party: $20.99 per Child</b></u><br>
All of the Giraffe Package, plus:
<ul>
	<li>25 Tokens per child</li>
	<li>FREE return pass for Birthday Kid</li>
	<li>Invitations &amp; Thank you cards</li>
</ul>

You can reserve a private birthday party room that can accommodate up to 32 guests for 2 hours with a $30 deposit that will be credited back to you on the day of your birthday party (non refundable if you cancel). Simply fill out the form below to place a 48 hour courtesy hold on your reservation. Please contact us at (602) 274-4653 to secure your reservation. You will also be able to secure your reservation using Paypal or a credit card if you want to. Please remember that <strong>no outside food or drink</strong> is allowed at Jambo! Park (except birthday cakes). Private birthday party room reservation requires a <strong>10 guest minimum</strong>. There is no admission fee to enter Jambo! Park, but if grown-ups want to participate in the fun, Adult Wristbands are available for just $8, and include most of our attractions!

<hr />

<center>
<br><br>
New Birthday Party <b>Private Room</b> Reservation</center></div> <div class="box"> 
<table align=center cellspacing="0" cellpadding="0" border="1" width="500">


<tr><td>
<form action="birthday_room.php?view=confirmation" method="post" onsubmit="return validate()">
<input type="hidden" name="paid" value="no">
<input type="hidden" name="tr" value="r">
<input type="hidden" name="room" value="1">
<input type="hidden" name="tble" value="<?php echo "$ambassador"?>">
<tr><td>Date: </td><td><input type="text" name="party_date" id="selected_date"><br></td></tr>
<tr><td>Time: </td><td id="time_dropdown">
<select name="party_time" id="party_time" >
	<option>8.00pm</option>
</select>
<br></td></tr>
<tr><td>Child's name: </td><td><input type=text name=child id="child"><br></td></tr>
<tr><td>Child's age: </td><td><input type=text name=child_age id="child_age"><br></td></tr>
<tr><td>Child's birthday: </td><td>
<select name="birth_year" id="birth_year">
	<option>Year</option>
<?php
$current_year = date("Y", time());
$current_year2 = date('Y', strtotime('-1 year'));
$current_year3 = date('Y', strtotime('-2 year'));
$current_year4 = date('Y', strtotime('-3 year'));
$current_year5 = date('Y', strtotime('-4 year'));
$current_year6 = date('Y', strtotime('-5 year'));
$current_year7 = date('Y', strtotime('-6 year'));
$current_year8 = date('Y', strtotime('-7 year'));
$current_year9 = date('Y', strtotime('-8 year'));
$current_year10 = date('Y', strtotime('-9 year'));
$current_year11 = date('Y', strtotime('-10 year'));
$current_year12 = date('Y', strtotime('-11 year'));
$current_year13 = date('Y', strtotime('-12 year'));
$current_year14 = date('Y', strtotime('-13 year'));
echo "<option value=$current_year>$current_year</option>";
echo "<option value=$current_year2>$current_year2</option>";
echo "<option value=$current_year3>$current_year3</option>";
echo "<option value=$current_year4>$current_year4</option>";
echo "<option value=$current_year5>$current_year5</option>";
echo "<option value=$current_year6>$current_year6</option>";
echo "<option value=$current_year7>$current_year7</option>";
echo "<option value=$current_year8>$current_year8</option>";
echo "<option value=$current_year9>$current_year9</option>";
echo "<option value=$current_year10>$current_year10</option>";
echo "<option value=$current_year11>$current_year11</option>";
echo "<option value=$current_year12>$current_year12</option>";
echo "<option value=$current_year13>$current_year13</option>";
echo "<option value=$current_year14>$current_year14</option>";
?>
</select>
<select name="birth_month" id="birth_month">
	<option>Month</option>
	<option value="01">January</option>
	<option value="02">February</option>
	<option value="03">March</option>
	<option value="04">April</option>
	<option value="05">May</option>
	<option value="06">June</option>
	<option value="07">July</option>
	<option value="08">August</option>
	<option value="09">September</option>
	<option value="10">October</option>
	<option value="11">Nobember</option>
	<option value="12">December</option>
</select>
<select name="birth_day" id="birth_day">
	<option>Day</option>
	<option value="01">1</option>
	<option value="02">2</option>
	<option value="03">3</option>
	<option value="04">4</option>
	<option value="05">5</option>
	<option value="06">6</option>
	<option value="07">7</option>
	<option value="08">8</option>
	<option value="09">9</option>
	<option value="10">10</option>
	<option value="11">11</option>
	<option value="12">12</option>
	<option value="13">13</option>
	<option value="14">14</option>
	<option value="15">15</option>
	<option value="16">16</option>
	<option value="17">17</option>
	<option value="18">18</option>
	<option value="19">19</option>
	<option value="20">20</option>
	<option value="21">21</option>
	<option value="22">22</option>
	<option value="23">23</option>
	<option value="24">24</option>
	<option value="25">25</option>
	<option value="26">26</option>
	<option value="27">27</option>
	<option value="28">28</option>
	<option value="29">29</option>
	<option value="30">30</option>
	<option value="31">31</option>
</select>
</td></tr>
<tr><td>Parent's name: </td><td><input type=text name=parent id="parent"><br></td></tr>
<tr><td>Parent's address: </td><td><input type=text name=parent_address id="parent_address"><br></td></tr>
<tr><td>Parent's city: </td><td><input type=text name=parent_city id = "parent_city"><br></td></tr>
<tr><td>Parent's zipcode: </td><td><input type=text name=parent_zip id="parent_zip"><br></td></tr>
<tr><td>Phone: </td><td><input type=text name=phone id="phone"><br></td></tr>
<tr><td>email: </td><td><input type=text name=email id="email"><br></td></tr>
<tr><td>Birthday Package: </td><td>
<select name="package" id="package">
  <option value="Monkey">Monkey $12.99/child</option>
  <option value="Giraffe">Giraffe $15.99/child</option>
  <option value="Elephant">Elephant $20.99/child</option>
</select>
<br></td></tr>
<tr><td>Number of Kids: </td><td><input type=text name=kids id="kids"><br></td></tr>
<tr><td>How did you find us: </td><td><input type=text name=source id ="source"><br></td></tr>
<tr><td>comment: </td><td><input type=text name=comment id="comment"><br></td></tr>
<tr><td>Jambo! Ambassador: </td><td><input disabled type=text name=ambassador id="ambassador" value="<?php echo "$ambassador"?>"><br></td></tr>
<tr><td></td><td><input type="submit" value="Next>>> Review your birthday party reservation"></td></tr>
</form>

</table>


<br><br><center><font size=2>The following are included with all 3 Birthday Party packages: Unlimited Himalaya Mini Roller Coaster Rides, Unlimited Monkey Mayhem Barrels Rides, Unlimited Spinning Tops Rides, Unlimited Flying Elephant Rides, Unlimited Jungle Swing Rides, Unlimited Safari Train Rides, Unlimited Indoor Playground with slides and tunnels, Unlimited Laser Tag and Unlimited 6-hole Indoor Miniature Golf.</center></font>

</div>
</div>
<script src="http://jambopark.com/party_reservation/js/jquery-1.10.2.js"></script>
<script src="http://jambopark.com/party_reservation/js/jquery-ui.js"></script>
<script>
/*/*$(function() {
//alert("JQuery is working");	
$("#selected_date").datepicker({
showOtherMonths: true,
selectOtherMonths: true, dateFormat:"yy-mm-dd",
beforeShowDay:
function(dt)
{
return [dt.getDay() == 2 || dt.getDay() == 4 ? false : true];
}
});

$("#check-btn-disabled").click(function(){
	
	var selected_date = $("#selected_date").val();
	var selected_time = $("#selected_time").val();
	if(selected_date != "") {
		
		//alert(selected_date);
		/*  var time_array  = new Array;
			var i = 0;
			$("#selected_time option").each(function() {
			 
			  time_array[i] = $(this).val();
			  
			  i++;		 
				   
			}); 
		
		$.post( "time.php", { selected_date: selected_date, time: selected_time })
		         .done(function( data ) {
	                 alert( "Data Loaded: " + data );
		});
	
		
	}	
});

$("#selected_date").change(function(){
	
	var selected_date = $("#selected_date").val();
	if(selected_date != "") {
		
		$.post( "time.php", { selected_date: selected_date })
		         .done(function( data ) {
	                // alert( "Data Loaded: " + data );
	               // $("#time_dropdown").html(data);
	               var data_val = $.trim(data);	               
	               var splitted_options = data_val.split(",");
	               $('#party_time option').remove();
	               for(var i = 0; i<splitted_options.length; i++){
					    
					    if(splitted_options[i] != "") {						
							
							$("#party_time").append(new Option(splitted_options[i], splitted_options[i]));
						}
				   }
		});
		
		
	}
	
});

});*/
</script>
<?php } ?>
<?php //include('../php/footer.php'); ?>

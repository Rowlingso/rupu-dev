<?php

define('ENCRIPTION_KEY', '#$%^&*()(*');


require_once 'db_functions.php';
/*$user = Register::sendClientEmail("12");

var_dump($user);*/

class Register
{
	public static function affiliateAgreement()
	{
		$agreementbody = "<h4>Terms of Service Agreement</h4>";
		$agreementbody .= "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>";
		$agreementbody .= "<input type=\"checkbox\" id=\"checkbox\"> I confirm the terms of service<br>";
		$agreementbody .= "<input type=\"submit\" id=\"submit\"  value=\"Submit\" onclick=\"post()\"> ";

		$agreementbody .= "<script>$(document).ready(function() {
		    $('#submit').prop('disabled', true); // disabled by default

		    $('#checkbox').click(function() {
		       // change on checkbox click
		       $('#submit').prop('disabled', !$('#checkbox').prop('checked'));
		    });

		});</script>";

		return $agreementbody;
		
	}
	
	public static function validateUserEntries($data)
	{

		//echo '<pre>';var_dump($_POST);exit;
		$err = "";
		$message = "";

		if(!$data['username'])
			$err="Please Enter Your Desired Username<br>";
		if(!$data['password'])
			$err="Please Enter Desired Password<br>";
		if(!$data['firstname'])
			$err="Please Enter First Name";
		if(!$data['lastname'])
			$err="Please Enter Last Name";
		if(!$data['organization'])
			$err="Please Enter Organization's name<br>";
		if(!$data['address'])
			$err="Please Enter address<br>";
		if(!$data['phone'])
			$err="Please Enter phone<br>";
		if(!$data['email'])
			$err="Please Enter email<br>";

		return $err;
	}

	public static function checkUserExists($username)
	{
		$users = DBFunctions::select('scraffiliateusr','*','usrnam="'.$username.'"');

		return $users->num_rows;
	}

	public static function storeToSession($data)
	{
		$_SESSION['post'] = $data;

		//echo '<pre>';var_dump($_SESSION['post']);exit;
	}

	public static function storeToDB($data)
	{
		$store['id'] = "''";
		$store['act'] = 0;
		$store['username'] = "'$data[username]'";
		$hashPassword = Register::hashPassword($data['password'],ENCRIPTION_KEY);
		$store['password'] = "'$hashPassword'";
		$store['firstname'] = "'$data[firstname]'";
		$store['lastname'] = "'$data[lastname]'";
		$store['organization'] = "'$data[organization]'";
		$store['address'] = "'$data[address]'";
		$store['phone'] = "'$data[phone]'";
		$store['email'] ="'$data[email]'";
		$timestamp = time();
		$store['usractkey'] = "'$timestamp'";//timestamp
		$store['usrip'] = "'$_SERVER[REMOTE_ADDR]'";
		$joindate = date("Y-m-d H:i:s" , time());
		$store['usrjondtetme'] = "'$joindate'";
		$store['usrinvby'] = "'$data[invcod]'";
		$store['usrinvsndmax'] = 10;
		$token = Register::generateToken();
		$store['token'] = "'$token'";
		$store['confirmflag'] = 0;
		$store['userlevel'] = "1";

		$saveid = DBFunctions::insert('scraffiliateusr',$store);

		//insert affiliate table entry

		Register::insertAffiliateStatus($saveid);


		//session_destroy();

		return $saveid;
	}

	//sets to unapproved by default
	public static function insertAffiliateStatus($saveid){

		$status['id'] = "''";
		$status['ambid'] = "'$saveid'";
		$status['approve'] = "'0'";

		DBFunctions::insert('ambassador_approvals',$status);

	}



		private static function hashPassword($pure_string, $encryption_key) {
	    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
	    return $encrypted_string;
	}

	function decrypt($encrypted_string, $encryption_key) {
	    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
	    return $decrypted_string;
	}

	private static function generateToken()
	{
		$ref 	= str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',10);
		return substr(str_shuffle($ref),0,15);	
	}

	public static function sendClientEmail($clientid)
	{
		$client = DBFunctions::select('scraffiliateusr','*','usrid='.$clientid.'');

		$row = $client->fetch_array(MYSQLI_ASSOC);


		$to  = $row['usremail'];

		// subject
		$subject = 'Registration Confirmation';

		// message
		$message = '
		<html>
		<body>
		  <p>Hi '.$row["usrnam"].' ,</p>
		  <p>We have received your registration request. Please click the link below to confirm your email address.</p>
		  <p>http://example.com/index.php?page=act/register&id='.$row["usrid"].'&token='.$row['token'].'</p>
		</body>
		</html>
		';

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'From: Jambopark Admin <orowlings@gmail.com>' . "\r\n";

		// Mail it
		$bool = mail($to, $subject, $message, $headers);

		return $bool;
	}

	public static function sendAdminEMail($clientid)
	{

		$client = DBFunctions::select('scraffiliateusr','*','usrid='.$clientid.'');

		$row = $client->fetch_array(MYSQLI_ASSOC);



		$to  = "admin@jambopark.com";

		// subject
		$subject = 'New User Registration Notification';

		// message
		$message = '
		<html>
		<body>
		  <p>Hi Administrator ,</p>
		  <p>A new user by the name '.$row["usrfnam"].' '.$row['usrlnam'].' has just registered.</p>
		</body>
		</html>
		';

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'From: Jambopark Admin <orowlings@gmail.com>' . "\r\n";

		// Mail it
		$bool = mail($to, $subject, $message, $headers);

		return $bool;

	}

	public static function confirmUser($id,$token)
	{
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		$query = "UPDATE scraffiliateusr SET confirmflag = 1 WHERE usrid = '".$id."' AND token = '".$token."'";

		return $mysqli->query($query);

	}


}
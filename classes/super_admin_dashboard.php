<?php

require_once 'db_functions.php';

class SuperAdminDashboard
{

  public static function getGeneralStats()
  {
      $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

      $query = "SELECT usrid, usrnam from scraffiliateusr";

      $result = $mysqli->query($query);

      $generalstats = array();

      $sorter = array();

      while($row = mysqli_fetch_array($result)) {
        //get clicks count
        $row['clickcount'] = SuperAdminDashboard::getCount('clicks','affiliate_count','usrid',$row['usrid']);
        //get reservations count
        $row['reservationscount'] = SuperAdminDashboard::getCount('reserves','reservations','ambid',$row['usrid']);
        //get paid deposits
         $row['paidcount'] = SuperAdminDashboard::getCount('clicks','affiliate_count','usrid',$row['usrid'],1);
         //get commissions
         $row['commissions'] = $row['paidcount'] * 10;
         //get all paid payments
         $row['paymentsent'] = SuperAdminDashboard::getPaymentSent($row['usrid']);

        $generalstats[] = $row;
      }

      return $generalstats;
  }

 public static function fetchAmbassadors($orderby='usrid')
 {

 	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		$query = "SELECT s.*, a.approve FROM scraffiliateusr as s left join ambassador_approvals as a on s.usrid = a.ambid order by s.$orderby asc";

    //echo $query;exit;

		$result =	$mysqli->query($query);

 	$ambassadors = array();

 	while($row = mysqli_fetch_array($result)) {
 		$ambassadors[] = $row;
	}

 	//$rows = $ambassadors->fetch_array(MYSQLI_ASSOC);

 	return $ambassadors;
 }

 public static function approveAmbassador($id,$token)
 {
 	$updatevalue = array('approve'=>'1');
 	DBFunctions::update('ambassador_approvals',$updatevalue,'ambid',$id);

  }

   public static function suspendAmbassador($id,$token)
 {
 	$updatevalue = array('approve'=>'2');
 	DBFunctions::update('ambassador_approvals',$updatevalue,'ambid',$id);

  }

  public static function displayPayments()
  {

  	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

  	$query = "SELECT s.usrfnam, s.usrlnam, p.* FROM affiliate_payment as p left join scraffiliateusr as s on s.usrid = p.affiliate order by s.usrid desc";


		$result =	$mysqli->query($query);

 	$payments = array();

 	while($row = mysqli_fetch_array($result)) {
 		$payments[] = $row;
	}

	return $payments;

  }

   public static function displayClicks()
  {

    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $query = "SELECT s.usrfnam, s.usrlnam, c.* FROM affiliate_count as c left join scraffiliateusr as s on s.usrid = c.usrid order by s.usrid desc";

    $result = $mysqli->query($query);

  $clicks = array();

  while($row = mysqli_fetch_array($result)) {
    $clicks[] = $row;
  }

  return $clicks;

  }

  public static function getCount($column,$table,$condition,$affiliateid, $paid = 0)
  {
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $query = "SELECT count($column) as $column from $table where $condition = $affiliateid";
    if($paid)
      $query .= " AND paid=1";

    $result = $mysqli->query($query);

    $result = $result->fetch_array(MYSQLI_ASSOC);

    return $result[$column];
  }

  public static function getPaymentSent($affiliateid)
  {
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    $query = "SELECT sum(amount) as amount from affiliate_payment where affiliate = $affiliateid";

    $result = $mysqli->query($query);

    $result = $result->fetch_array(MYSQLI_ASSOC);

    return $result['amount'];
  }

 public static function logout()
    {
      unset($_SESSION);
      session_destroy();

      header('Location: '.SuperAdminDashboard::url().'/index.php?page=act/login&login=2' );
    }

    public static function url(){
  return sprintf(
    "%s://%s%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME'],
    dirname($_SERVER['PHP_SELF'])
  );
}
}
<?php

require_once 'db_functions.php';


class Reservations
{

	public static function displayConfirmation($data)
	{
		$data['child_bd'] = $data['birth_year']."-".$data['birth_month']."-".$data['birth_day'];

		$confirmation = "<br><b>Please review your private birthday party room reservation</b><br>
						 Party Date: $data[party_date]<br>
						 Party Time: $data[party_time]<br>
						 Child's Name: $data[child]<br>
						 Child's Age: $data[child_age]<br>
						 Child's Birthday: $data[child_bd]<br>
						 Parent's Name: $data[parent]<br>
						 Parent's Address: $data[parent_address], $data[parent_city] $data[parent_zip]<br>
						 Parent's Phone: $data[phone]<br>
						 Parent's email: $data[email]<br>
						 Selected Birthday Package: $data[package]<br>
						 Number of Kids: $data[kids]<br>
						 How did you hear about us: $data[source]<br>
						 Comment: $data[comment]<br>


						 You are reserving a private room that can accomodate 32 guests for 2 hours. You are placing a 48 hour courtesy hold on your room. Please contact us at (602) 274-4653 to secure your private room. You are also agreeing to our rules: <br>";

		$confirmation .= "* 10 guest minimum<br>
						* Children must be supervised at all times<br>
						* No running or climbing<br>
						* Socks required in playground<br>
						* Outside food and beverages are not allowed<br>
						* Visa, Mastercard, American Express and Discover Card accepted<br>
						* Cash or credit card only, sorry, no checks accepted<br>
						* All prices are exclusive of local sales taxes<br>
						* Prices are subject to change at any time without notice<br>";

		$confirmation .= "<input type=\"checkbox\" id=\"checkbox\"> I confirm the terms of service<br>";
		$confirmation .= "<input type=\"submit\" id=\"submit\"  value=\"Submit\" onclick=\"post()\"> ";

		$confirmation .= "<script>

		function post(){
		  location.href='http://localhost/affiliate/birthday_room.php?action=reserve';
		}


		$(document).ready(function() {
		    $('#submit').prop('disabled', true); // disabled by default

		    $('#checkbox').click(function() {
		       // change on checkbox click
		       $('#submit').prop('disabled', !$('#checkbox').prop('checked'));
		    });

		});</script>";

		$confirmation .= "<br>You can also secure your private room now by paying the $30 deposit via Paypal.<br>
		<input type='image' src='https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif' border='0' name='submit' alt='PayPal - The safer, easier way to pay online!'>
		<img alt='' border='0' src='https://www.paypalobjects.com/en_US/i/scr/pixel.gif' width='1' height='1'>";

			return $confirmation;

		
	}

	public static function sendToPaypal($data)
	{


		echo "<form action=birthday_room4.php method=post>";
		echo "<input type=hidden name='next_id' value='$next_id'>";
		echo "<input type=hidden name='expire' value='$expire'>";
		echo "<input type=hidden name='expire2' value='$expire2'>";
		echo "<input type=hidden name='paid' value='no'>";
		echo "<input type=hidden name='tr' value='r'>";
		echo "<input type=hidden name='party_date' value='$party_date'>";
		echo "<input type=hidden name='party_time' value='$party_time'>";
		echo "<input type=hidden name='room' value='$room'>";
		echo "<input type=hidden name='tble' value='$tble'>";
		echo "<input type=hidden name='child' value='$child'>";
		echo "<input type=hidden name='child_age' value='$child_age'>";
		echo "<input type=hidden name='child_bd' value='$child_bd'>";
		echo "<input type=hidden name='parent' value='$parent'>";
		echo "<input type=hidden name='parent_address' value='$parent_address'>";
		echo "<input type=hidden name='parent_city' value='$parent_city'>";
		echo "<input type=hidden name='parent_zip' value='$parent_zip'>";
		echo "<input type=hidden name='phone' value='$phone'>";
		echo "<input type=hidden name='email' value='$email'>";
		echo "<input type=hidden name='package' value='$package'>";
		echo "<input type=hidden name='kids' value='$kids'>";
		echo "<input type=hidden name='source' value='$source'>";
		echo "<input type=hidden name='comment' value='$comment'>";
		echo "<input type='image' src='https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif' border='0' name='submit' alt='PayPal - The safer, easier way to pay online!'>";
		echo "<img alt='' border='0' src='https://www.paypalobjects.com/en_US/i/scr/pixel.gif' width='1' height='1'>";
		echo "</form>";
		
	}

	public static function storeReservations()
	{

		//check if the request has an ambassador's token, if so, store, else, invalid request.

		if($_SESSION['token'])
		{

		$data = $_SESSION['reservation'];

		$store['id'] = "''";
		$store['date'] = "'$data[party_date]'";
		$store['child_name'] = "'$data[child]'";
		$store['child_age'] = "'$data[child_age]'";
		$childbday = $data['birth_year']."-".$data['birth_month']."-".$data['birth_day'];
		$store['child_bday'] = "'$childbday'";
		$store['parent_name'] = "'$data[parent]'";
		$store['parent_address'] = "'$data[parent_address]'";
		$store['parent_city'] = "'$data[parent_city]'";
		$store['parent_zip'] = "'$data[parent_zip]'";
		$store['phone'] ="'$data[phone]'";
		$store['email'] = "'$data[email]'";//timestamp
		$store['bdaypackage'] = "'$data[package]'";
		$store['no_of_kids'] = "'$data[kids]'";
		$store['how_you_found'] = "'$data[source]'";
		$store['comment'] = "'$data[comment]'";
		$store['ambid'] = "'$_SESSION[ambassadorid]'";
		$store['paid'] = "0";
		$datetime = date('Y-m-d H:i:s');
		$store['time'] = "'$datetime'";
		$store['reserves'] = "'1'";

		$saveid = DBFunctions::insert('reservations',$store);

		return $saveid;
	}else{
		die('invalid request');
	}

	}


}